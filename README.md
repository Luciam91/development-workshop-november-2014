November 2014 - Development Workshop Demo
=========================================

**Anything with an (@) symbol requires the space directly after it removed, Bitbucket auto-attempts to target users and repositories if you take out the space**

# Node
Before you can run any of the below commands you will need to have Node.js installed on your machine, to do this simply run:
> brew install node

# Bower
Bower is the equivalent of composer for front-end dependencies. Thus removing the need to maintain front-end dependencies ourselves.

Using Bower we can get all front-end dependencies out of our git repository and just retrieve them on build/deployment.

# SASS

### [SASS Reference](http://sass-lang.com/documentation/file.SASS_REFERENCE.html)


### Imports
One of the main features of SASS I want to use is the import command. This allows us to break up all of our SCSS files into smaller chunks and then have a "main" file which we just import into
> @ import "filename";

### Extend
We can use extend to work in a similar fashion to a PHP class extend and give the same CSS values in another class. An example is below
> .error-message { 
>    background: red; 
> }
>
> .serious-error-message {
>   @ extend .error-message; 
>   font-weight: bold;
> }

The above example would compile to:
> .error-message, .serious-error-message {
>   background: red;
> }
>
> .serious-error-message {
>   font-weight: bold;
> }

### Mixins
The best way to think of a mixin is a CSS function declaration for outputting repetitive tasks, for calculations there's an actual function operator (not using that here, but it's fairly simple to follow). And just like all other languages with functions, allows you to move repeated tasks into 1 easy to use line of code. The below mixin simply declares height and width values based on passed in variables. Mixins can be declared without variables as well.

> @ mixin heightWidth($height, $width: $height) { height: $height; $width: $width; }

And to use this:

> .serious-error { @ include heightWidth(100px, 500px); }

This will compile to:

> .serious-error { height: 100px; width: 500px; }

### Placeholders
I don't use any placeholders in this project but I do feel they really need mentioned. If you have a set of SCSS/CSS code which is re-used in more than 1 place, you might think you should use a mixin as above? Nope, unless you're passing a variable to it, you're better to use a placeholder. This is simply because a mixin will output the resulting css everywhere it is included. To avoid this use a placeholder, which is not compiled on it's own by SASS but compiles all relevant rules into 1. Placeholders are defined like a CSS class but with a % rather than a .

> %center { display: block; margin-left: auto; margin-right: auto; }
>
> .container { @ extend %center; }
>
> .image-cover { @ extend %center; }

Will compile to:

> .container, .image-cover { display: block; margin-left: auto; margin-right: auto; }

### Operators
SASS/SCSS also introduces some operators which everyone is use to: if, for and while. I have not spent a lot of time with these, but they work pretty much how you would expect an if statement and for/while loops to work.

# Gulp
To use the gulpfile you will need to install this from npm, unfortunately this has to be done globally and locally

## Global Install
> npm install -g gulp

## Local Install
> npm install gulp --save-dev

To install any other npm packages just follow the above rule replacing "gulp" with the name of the package (such as "gulp-watch")

## Tasks
Gulp runs by performing a programmer-defined task to perform a specific action. All tasks are defined in the gulpfile.js which is defined in the project root. There are only really 4/5 commands you need to know in Gulp to get a decent understanding of how it's going on.

I've written a gulpfile to allow us to minify our javascript, move files to a vendor folder and compile our SCSS files to CSS, but that's just a very basic level of what you can do. If you check the gulpfile you can see what each task is doing and how it's affected.

#### How a task is defined
Below I've defined a task called "sass", it doesn't do anything but this is how to define the task you want

> gulp.task('sass', function () { });

To run a task that you've already defined (in the command line)

> gulp sass

This will execute any commands you have within your sass command