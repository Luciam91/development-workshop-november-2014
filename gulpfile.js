var gulp = require('gulp'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    autoprefix = require('gulp-autoprefixer'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    vendor = require('gulp-concat-vendor');

var config = {
    sassPath: './scss',
    bowerDir: './javascripts/vendor'
}

gulp.task('default', function() {
    
});

gulp.task('scripts', function() {
    return gulp.src('./javascripts/*.js')
        .pipe(gulp.dest('./js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify({compress: true}))
        .pipe(gulp.dest('./public/js'));
});

gulp.task('bootstrap-css', function() {
  return gulp.src(config.bowerDir + '/bootstrap-sass-official/assets/stylesheets/**/*.scss')
    .pipe(gulp.dest('./scss/vendor'));
});

gulp.task('font-awesome', function() {
  return gulp.src(config.bowerDir + '/fontawesome/scss/**/*.scss')
    .pipe(gulp.dest('./scss/vendor'));
});

gulp.task('sass', function () {
    return gulp.src('./scss/main.scss')
            .pipe(sass())
            .pipe(gulp.dest('./public/css'));
});

gulp.task('icons', function() {
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*')
    .pipe(gulp.dest('./public/fonts'));
});

gulp.task('vendorjs', function() {
  gulp.src(config.bowerDir + '/**/*.js')
  .pipe(concat('vendor.js'))
  .pipe(gulp.dest('./public/js'));  
});

gulp.task('watch', ['icons', 'bootstrap-css', 'font-awesome', 'vendorjs', 'sass', 'scripts'], function() {
    gulp.watch('./javascripts/*.js', ['scripts']);
    gulp.watch('./scss/*.scss', ['sass']);
});